#include <iostream>
#include <string>


#include <QElapsedTimer>

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>

#include "yolodetector.h"

using namespace std;


#define SSTR(x) static_cast<std::ostringstream &>((std::ostringstream() << std::dec << x)).str()

const string pathToVideo ("/home/nvidia/Projects/yolotesting/Data/Run 100 m.mp4");

std::vector<cv::Rect> resultsYolo;

int main()
{
    YoloDetector* det = new YoloDetector();

    //cv::VideoCapture video(0);
    cv::VideoCapture video(pathToVideo);

    if(!video.isOpened())
    {
        cout << "Could not read video file" << endl;
        return EXIT_FAILURE;
    }
    cout << "Video opened!" << endl;

    cv::Mat frame;

    QElapsedTimer timer;

    while(true)
    {
        // Start timer

        timer.restart();
        video.read(frame);
        cout << "read frame" << endl;

        string frame_dim = "frame dimensions (WxH): " + to_string(frame.rows) + "x" + to_string(frame.cols);

        resultsYolo = det->Detect(frame);
        cout << "detected objects: " << resultsYolo.size() << endl;
        for (auto &r : resultsYolo)
        {
            cv::rectangle(frame, r, cv::Scalar(0,255,0), 2, 1);
        }

        // Calculate Frames per second (FPS)
        int timer_elapsed = timer.elapsed();


        // Display FPS on frame
        cv::putText(frame, frame_dim, cv::Point(10,25), cv::FONT_HERSHEY_SIMPLEX, 0.75, cv::Scalar(0,0,255), 2);
        cv::putText(frame, "Calc. time : " + SSTR(timer_elapsed), cv::Point(10,50), cv::FONT_HERSHEY_SIMPLEX, 0.75, cv::Scalar(0,0,255), 2);

        // Display frame.
        cv::imshow("Yolo", frame);

        // Exit if ESC pressed.
        if(cv::waitKey(1) == 27) break;
    }

    delete det;

    return 0;

}

