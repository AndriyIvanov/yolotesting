#include "yolodetector.h"

YoloDetector::YoloDetector(QObject *parent) : QObject(parent)
{
    _detector = new Detector(this->_cfg_path, this->_weight_path);
}

std::vector<cv::Rect> YoloDetector::Detect(cv::Mat frame)
{
    std::vector<cv::Rect> result;
    _resultsYolo = _detector->detect(frame, _TRESHOLD);
    //std::cout << "Detected in Yolo: " << _resultsYolo.size() << std::endl;
    for (auto &r : _resultsYolo)
    {
        if(r.prob > _TRESHOLD && r.obj_id == _objType)
        {
            result.push_back(cv::Rect(r.x, r.y, r.w, r.h));
        }
    }
    return result;
}

YoloDetector::~YoloDetector()
{
    delete _detector;
}

