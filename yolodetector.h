#ifndef YOLODETECTOR_H
#define YOLODETECTOR_H

#include <string>
#include <QObject>

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>

#include "yolo_v2_class.hpp"

enum TrackedObjects{Person, Bicycle, Car, Motorbike, Aeroplane, Bus, Train, Truck, Boat};

class YoloDetector : public QObject
{
    Q_OBJECT

    const std::string _cfg_path = "/home/nvidia/Downloads/darknet-master/cfg/yolov3.cfg";
    const std::string _weight_path = "/home/nvidia/Downloads/darknet-master/yolov3.weights";

    double _TRESHOLD = 0.8;                                //Probability threshold for Yolo detection
    TrackedObjects _objType = Person;

    Detector* _detector;
    std::vector<bbox_t> _resultsYolo;

public:
    explicit YoloDetector(QObject *parent = 0);
    std::vector<cv::Rect> Detect(cv::Mat frame);
    ~YoloDetector();

signals:

public slots:
};

#endif // YOLODETECTOR_H
