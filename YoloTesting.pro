QT += core
QT -= gui

TARGET = YoloTesting
CONFIG += console
CONFIG -= app_bundle
CONFIG += link_pkgconfig

QMAKE_CXXFLAGS += -std=c++14

INCLUDEPATH +='/usr/include/aarch64-linux-gnu/qt5/'

INCLUDEPATH += `pkg-config --cflags opencv`
LIBS += `pkg-config --libs opencv`

INCLUDEPATH += /home/nvidia/Downloads/darknet-master/src/
INCLUDEPATH += /home/nvidia/Downloads/darknet-master/include/
LIBS += -L'/home/nvidia/Downloads/darknet-master' -ldarknet


TEMPLATE = app

SOURCES += main.cpp \
    yolodetector.cpp

HEADERS += \
    yolodetector.h

